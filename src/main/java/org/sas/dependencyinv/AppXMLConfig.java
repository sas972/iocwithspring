package org.sas.dependencyinv;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class AppXMLConfig {

	public static void main(String[] args){
		try (ConfigurableApplicationContext context = new FileSystemXmlApplicationContext("shopcontext.xml")){
			Shop shop = context.getBean(Shop.class);
			shop.open();
			shop.printPrices();
		}
	}

}
