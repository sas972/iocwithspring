package org.sas.dependencyinv;

import java.math.BigDecimal;

public interface PriceCalculator {

	BigDecimal price(Product prod);
}
