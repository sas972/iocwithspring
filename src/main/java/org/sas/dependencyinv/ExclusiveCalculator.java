package org.sas.dependencyinv;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Component
@Qualifier("exclusive")
public class ExclusiveCalculator implements PriceCalculator {

	public BigDecimal price(Product prod) {
		return prod.getPrice().multiply(BigDecimal.valueOf(2.5));
	}

}
