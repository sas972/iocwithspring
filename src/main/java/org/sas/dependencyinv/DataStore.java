package org.sas.dependencyinv;

public interface DataStore<T> {
	void add(T a);
	Iterable<T> list();
}
