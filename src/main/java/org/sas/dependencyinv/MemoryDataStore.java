package org.sas.dependencyinv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class MemoryDataStore<T> implements DataStore<T> {
	List<T> list = new ArrayList<>();

	@Override
	public void add(T a) {
		list.add(a);
	}

	@Override
	public Iterable<T> list() {
		return Collections.unmodifiableCollection(list);
	}
}
