package org.sas.dependencyinv;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("org.sas.dependencyinv")
public class AppAnnotationConfig {

	public static void main(String[] args) {
		try(ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(AppAnnotationConfig.class)){
			Shop shop = context.getBean(Shop.class);
			shop.open();
			shop.printPrices();
		}
	}

}
