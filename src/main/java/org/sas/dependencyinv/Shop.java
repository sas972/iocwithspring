package org.sas.dependencyinv;

import java.math.BigDecimal;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;


@Configuration
public class Shop {
	@Inject
	private DataStore<Product> dataStore;
	
	@Autowired
	@Qualifier("exclusive")
	private PriceCalculator priceCalculator;
	
	public Shop(){};
	
	public Shop(DataStore<Product> datastore, PriceCalculator priceCalculator) {
		super();
		this.dataStore = datastore;
		this.priceCalculator = priceCalculator;
	}
	
	public void open() {
		dataStore.add(new Product("Book", new BigDecimal("100")));
		dataStore.add(new Product("UberLaptop", new BigDecimal("10000")));
    }
	
	public void printPrices(){
		for(Product p : dataStore.list() ){
			System.out.print("Name: " +  p.getName());
			System.out.println(" price: " +  priceCalculator.price(p));
		}
	}
}
