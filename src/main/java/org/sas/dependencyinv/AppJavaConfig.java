package org.sas.dependencyinv;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

public class AppJavaConfig {

	@Bean
	Shop createShop(){
		return new Shop(createDataStore(), ceateCalculator());
	}
	
	PriceCalculator ceateCalculator(){
		return new DiscountCalculator();
	}
	
	DataStore<Product> createDataStore(){
		return new MemoryDataStore<>();
	}
	
	public static void main(String[] args) {
		try( ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(AppJavaConfig.class)){
			Shop shop = context.getBean(Shop.class);
			shop.open();
			shop.printPrices();
		}

	}

}
